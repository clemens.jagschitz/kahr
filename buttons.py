from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.uix.image import Image

class StartButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(StartButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/start_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/start_button_not_pressed.png"

class OptionButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(OptionButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/options_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/options_button_not_pressed.png"

class ExitButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(ExitButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/exit_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/exit_button_not_pressed.png"

class BackButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(BackButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/back_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/back_button_not_pressed.png"

class SaveButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(SaveButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/save_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/save_button_not_pressed.png"
