
#pickle

from kivy.config import Config
Config.set('graphics', 'width', '704')
Config.set('graphics', 'height', '480')
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'left', 100)
Config.set('graphics', 'top',  100)
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
Config.write()

#################################################################################################

from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
import random
import time
from kivy.vector import Vector
#from kivy.uix.label import Label
#from kivy.uix.popup import Popup
from kivy.uix.image import Image, AsyncImage
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.properties import StringProperty
from kivy.core.window import Window
#from kivy.animation import Animation
from kivy.uix.boxlayout import BoxLayout
#from kivy.uix.textinput import TextInput
from kivy.properties import ObjectProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import (NumericProperty, ReferenceListProperty,
                             ObjectProperty)

from world import world1
from world2 import world2
from buttons import *
#Builder.load_file('heme.kv')
from time import sleep


##############################################################################################################################

class GameTileMap(Image):
    pass

class Tile(Widget):
    pass

class MenuScreen(Screen):
    pass

class SettingsScreen(Screen):
    pass

class CharacterCreationScreen(Screen):
    pass

class Character(Widget):
    pass

class Coll(Widget):
    pass

#################################################################################################

gameroot = FloatLayout()
rootCollision = FloatLayout()

class GameScreen(Screen):
    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)
        self.z = 0
        self.b = 448
        self.cz = 0
        self.cb = 448
        self.badTiles = []
        self.tiles = [Tile(pos=(self.x, self.y))]
        self.wimg = Basic(source='Resources/sprite.png', size = (20,20), pos=(200, 100), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
        self.activeMap = world2


    def playy(self):
        gameroot.add_widget(self.wimg)
        Clock.schedule_interval(self.wimg.update, 1.0 / 60.0)

    def worldgen(self):
        self.add_widget(gameroot)
        self.add_widget(rootCollision)
        if 1 == 1:
            for tile in self.activeMap:
                if tile == 'X':
                    mapX = GameTileMap(size = (32,32), source = "Resources/tile_water.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(mapX)

                elif tile == '0':
                    map0 = GameTileMap(size = (32,32), source = "Resources/tile_stone.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map0)

                elif tile == '3':
                    map3 = GameTileMap(size = (32,32), source = "Resources/stairs_down.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map3)

                elif tile == '1':
                    map1 = GameTileMap(size = (32,32), source = "Resources/items/bomb.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map1)

                elif tile == '2':
                    map2 = GameTileMap(size = (32,32), source = "Resources/items/blue_drank.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map2)

                elif tile == '4':
                    map4 = GameTileMap(size = (32,32), source = "Resources/items/green_drank.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map4)

                elif tile == '5':
                    map5 = GameTileMap(size = (32,32), source = "Resources/items/red_drank.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map5)

                elif tile == '6':
                    map6 = GameTileMap(size = (32,32), source = "Resources/items/yellow_drank.png", pos = (self.z, self.b), size_hint=(None, None), allow_stretch=True, keep_ratio=False)
                    self.z += 32
                    self.tiles.append(map6)

                elif tile == '\n':
                    self.z = 0
                    self.b -= 32

        if 1 == 1:
            for tile in self.activeMap:
                if tile == 'X':
                    mapC = Coll(size = (32,32), pos = (self.cz, self.cb), size_hint=(None, None))
                    self.cz += 32
                    self.badTiles.append(mapC)

                elif tile == '0':
                    self.cz += 32

                elif tile == '3':
                    self.cz += 32

                elif tile == '1':
                    self.cz += 32

                elif tile == '2':
                    self.cz += 32

                elif tile == '4':
                    self.cz += 32

                elif tile == '5':
                    self.cz += 32

                elif tile == '6':
                    self.cz += 32

                elif tile == '\n':
                    self.cz = 0
                    self.cb -= 32


        for tiler in self.tiles:
            print(tiler, "added at", tiler.pos)
            gameroot.add_widget(tiler)

        for til in self.badTiles:
            print(til, "added at", til.pos)
            rootCollision.add_widget(til)

        #print(self.tiles)
        #print(self.badTiles)

    def on_enter(self):
        print ("on_enter fired")
        self.worldgen()
        self.playy()

#################################################################################################

class Basic(Image):

    pFrame = 0

    def __init__(self, **kwargs):
        super(Basic, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)
        self.collid = True
        self.pressed_keys = set()

        self.pressed_actions = {
            'w': lambda: self.move_up(),
            's': lambda: self.move_down(),
            'a': lambda: self.move_left(),
            'd': lambda: self.move_right(),
            }

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard.unbind(on_key_up=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        self.pressed_keys.add(keycode[1])

    def _on_keyboard_up(self, keyboard, keycode):
        self.pressed_keys.remove(keycode[1])

    def move_up(self):
        if self.collid == False:
            self.y += 1
            print (self.collid)

    def move_down(self):
        if self.collid == False:
            self.y -= 1
            print (self.collid)


    def move_left(self):
        if self.collid == False:
            self.x -= 1.2
            print (self.collid)


    def move_right(self):
        if self.collid == False:
            self.x += 1.2
            print (self.collid)


    def update(self, dt):
        for key in self.pressed_keys:
            try:
                self.pressed_actions[key]()
            except KeyError:
                print("Frame: %s Key %s pressed" % (self.pFrame, key))

        self.pFrame += 1
        #print(len(self.parent.parent.badTiles))
        #import pdb; pdb.set_trace()
        #print(self.x, self.y, self.width, self.height)
        for bad in self.parent.parent.badTiles:
            if self.parent.parent.wimg.collide_widget(bad):
                print ("collide with", bad)
                self.collid = True
            else:
                self.collid = False 

#################################################################################################

class KahrApp(App):

    title = 'kahr'
    icon = 'Resources/custoicon.png'

    def build(self):
        sm = ScreenManager()
        sm.add_widget(MenuScreen(name='menu'))
        sm.add_widget(SettingsScreen(name='settings'))
        sm.add_widget(CharacterCreationScreen(name='creationscreen'))
        sm.add_widget(GameScreen(name='gameplay'))
        return sm

    def save(self, plrname):
        fob = open('Resources/stats.txt','w+')
        fob.write(plrname + "\n")
        fob.close()


if __name__ == '__main__':
    KahrApp().run()
